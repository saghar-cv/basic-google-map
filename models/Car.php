<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%car}}".
 *
 * @property int $id
 * @property string $plaque
 * @property string $model
 * @property string $lat
 * @property string $lang
 */
class Car extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%car}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['plaque', 'model', 'lat', 'lang'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'plaque' => 'Plaque',
            'model' => 'Model',
            'lat' => 'Lat',
            'lang' => 'Lang',
        ];
    }
}
