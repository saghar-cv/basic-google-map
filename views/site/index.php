<?php

/* @var $this yii\web\View */

/* @var $cars Car[] */

use app\models\Car;
use dosamigos\google\maps\LatLng;
use dosamigos\google\maps\Map;
use dosamigos\google\maps\overlays\InfoWindow;
use dosamigos\google\maps\overlays\Marker;

$this->title = 'Map';


?>
<div id="marker">
    <?
    $coord = new LatLng(['lat' => 39.720089311812094, 'lng' => 2.91165944519042]);
    $map = new Map([
        'center' => $coord,
        'zoom' => 14,
        'width' => '100%'
    ]);

    foreach ($cars as $car) {
        $coord = new LatLng(['lat' => $car->lat, 'lng' => $car->lang]);
        $marker = new Marker([
            'position' => $coord,
            'title' => $car->plaque,
            'animation' => 'google.maps.Animation.DROP',
            'visible' => 'true'
        ]);
        $marker->attachInfoWindow(new InfoWindow(['content' => 'Plaque: ' . $car->plaque . '<br>' . 'Model: ' . $car->model]));
        $map->addOverlay($marker);
    }
    ?>
    <?= $map->display(); ?>
</div>

<?
$script = <<< JS
var auto_refresh = setInterval(
function () {
  $('#marker').load('#marker');
}, 6000);
JS;
$this->registerJs($script);
?>
