<?php

use yii\db\Migration;

/**
 * Class m190719_183055_create_table_car
 */
class m190719_183055_create_table_car extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%car}}', [
            'id' => $this->primaryKey(),
            'plaque' => $this->string(),
            'model' => $this->string(),
            'lat' => $this->string(),
            'lang' => $this->string()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%car}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190719_183055_create_table_car cannot be reverted.\n";

        return false;
    }
    */
}
